function bsearch(array, value) {
    var l = 0;
    var h = array.length-1;
    while (l <= h) {
        console.log("["+l+","+h+"]");
        var m = l+((h-l)>>1);
        if (value > array[m]) {
            l = m + 1;
        } else if (value < array[m]) {
            h = m - 1;
        } else if (l < m && value === array[m-1]) {
            h = m - 1;
        } else {
            return m;
        }
    }
    console.log("["+l+","+h+"]!");
    return -l-1;
}

function bs(array, value) {
    console.log("==========");
    var index = bsearch(array, value);
    console.log("bsearch(array, " + value + ") -> " + index);
}

var array = [0,3,8,10,11,11,11,17,20,22,23];
console.log("array = [" + array + "]");
bs(array, 3);
bs(array, 10);
bs(array, 11);
bs(array, 20);
bs(array, 9);
