package make10;

import java.util.List;

/**
 * 数字ノード
 */
public class NumberNode extends Node {

    private int number = -1;

    @Override
    public List<Integer> assingNumbers(List<Integer> numbers) {
        number = numbers.get(0);
        return numbers.subList(1, numbers.size());
    }

    @Override
    public Fraction evaluate() {
        return new Fraction(number, 1);
    }

    @Override
    public String toString() {
        return String.valueOf(number);
    }

    @Override
    public String[] dump() {
        return new String[] { String.format("─[%d]", number) };
    }

}
