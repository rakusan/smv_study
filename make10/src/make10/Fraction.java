package make10;

/**
 * 分数クラス。
 * 除算をその場で計算し浮動小数点数を得るのではなく、分数で表現する。
 * 除算以外の計算も分数のまま行う。また必要に応じて約分する。
 */
public class Fraction {

    @SuppressWarnings("serial")
    public static class ZeroDenominatorException extends ArithmeticException {}

    public final int numerator;
    public final int denominator;

    /**
     * @param numerator 分子
     * @param denominator 分母
     */
    public Fraction(int numerator, int denominator) {
        if (denominator == 0) {
            throw new ZeroDenominatorException();
        }
        // 分母が負にならないようにする。
        if (denominator < 0) {
            denominator = -denominator;
            numerator = -numerator;
        }
        // 約分する。
        int gcd = gcd(Math.abs(numerator), denominator);
        this.numerator = numerator / gcd;
        this.denominator = denominator / gcd;
    }

    /**
     * 最大公約数を求める
     * @return 最大公約数
     */
    private int gcd(int x, int y) {
        while (y != 0) {
            int t = x % y;
            x = y;
            y = t;
        }
        return x;
    }

    public Fraction add(Fraction other) {
        int gcd = gcd(denominator, other.denominator);
        int other_denominator_div_gcd = other.denominator / gcd;
        return new Fraction(
                other_denominator_div_gcd * numerator + denominator / gcd * other.numerator,
                other_denominator_div_gcd * denominator);
    }

    public Fraction sub(Fraction other) {
        int gcd = gcd(denominator, other.denominator);
        int other_denominator_div_gcd = other.denominator / gcd;
        return new Fraction(
                other_denominator_div_gcd * numerator - denominator / gcd * other.numerator,
                other_denominator_div_gcd * denominator);
    }

    public Fraction mul(Fraction other) {
        return new Fraction(numerator * other.numerator,
                            denominator * other.denominator);
    }

    public Fraction div(Fraction other) {
        return new Fraction(numerator * other.denominator,
                            denominator * other.numerator);
    }

    @Override
    public String toString() {
        return (denominator == 1) ? String.valueOf(numerator)
                                  : String.format("%d/%d", numerator, denominator);
    }

}
