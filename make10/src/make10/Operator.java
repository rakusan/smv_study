package make10;

/**
 * 演算子の列挙型
 */
public enum Operator {

    ADD("+") { public Fraction calculate(Fraction left, Fraction right) { return left.add(right); } },
    SUB("-") { public Fraction calculate(Fraction left, Fraction right) { return left.sub(right); } },
    MUL("*") { public Fraction calculate(Fraction left, Fraction right) { return left.mul(right); } },
    DIV("/") { public Fraction calculate(Fraction left, Fraction right) { return left.div(right); } };

    public final String symbol;

    Operator(String symbol) {
        this.symbol = symbol;
    }

    /**
     * この演算子定数に対応する演算を行う。
     */
    public abstract Fraction calculate(Fraction left, Fraction right);

    @Override
    public String toString() {
        return symbol;
    }

}
