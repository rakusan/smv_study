package make10;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import make10.Fraction.ZeroDenominatorException;
import util.PermutationIterator;
import util.RepeatedCombinationIterator;
import util.RepeatedPermutationIterator;

public class Make10 {

    public static void main(String[] args) {
        if (args.length == 0) {
            usage();
        } else {
            boolean all = false;
            boolean dump = false;
            boolean rpn = false;
            int i;
            for (i = 0; i < args.length; ++i) {
                if (args[i].equals("-all")) {
                    all = true;
                } else if (args[i].equals("-dump")) {
                    dump = true;
                } else if (args[i].equals("-rpn")) {
                    rpn = true;
                } else {
                    break;
                }
            }
            if (i != args.length-1 || args[i].length() == 0) {
                usage();
            } else {
                try {
                    int n = Integer.parseInt(args[i]);
                    if (n >= 0) {
                        make10(args[i], all, dump, rpn);
                    } else {
                        make10(-n, all, dump, rpn);
                    }
                } catch (NumberFormatException e) {
                    usage();
                }
            }
        }
    }

    private static void usage() {
        String canonicalName = Make10.class.getCanonicalName();
        System.err.println("nnnnを解く:");
        System.err.printf ("  java %s [-all] [-dump] [-rpn] nnnn%n", canonicalName);
        System.err.println();
        System.err.println("n桁の問題を全て解く:");
        System.err.printf ("  java %s [-all] [-dump] [-rpn] -n%n", canonicalName);
        System.err.println();
        System.exit(1);
    }

    private static void make10(String str, boolean all, boolean dump, boolean rpn) {
        List<Integer> input = new ArrayList<>(str.length());
        for (int i = 0; i < str.length(); ++i) {
            int num = str.charAt(i) - '0';
            if (num < 0 || num > 9) {
                throw new IllegalArgumentException();
            }
            input.add(num);
        }
        if (rpn) {
            make10rpn(input, all, dump);
        } else {
            make10(input, all, dump);
        }
    }

    private static void make10(int n, boolean all, boolean dump, boolean rpn) {
        Iterator<List<Integer>> it = new RepeatedCombinationIterator<>(
                Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), n);
        
        while (it.hasNext()) {
            if (rpn) {
                make10rpn(it.next(), all, dump);
            } else {
                make10(it.next(), all, dump);
            }
        }
    }

    private static void make10(List<Integer> input, boolean all, boolean dump) {
        int nNumbers = input.size();
        int nOperators = nNumbers - 1;
        
        if (nOperators == 0) {
            return;
        }
        
        // 演算子の数がnOperatorsの演算木
        OperatorNode tree = new OperatorNode(nOperators);
        
        do {
            // 演算子の重複順列を列挙するイテレータ
            Iterator<List<Operator>> operatorPerm = new RepeatedPermutationIterator<>(Operator.values(), nOperators);
            while (operatorPerm.hasNext()) {
                
                // イテレータから取り出した演算子のリストを演算木に設定する
                tree.assignOperators(operatorPerm.next());
                
                // 数字の順列を列挙するイテレータ
                Iterator<List<Integer>> numberPerm = new PermutationIterator<>(input);
                while (numberPerm.hasNext()) {
                    
                    // イテレータから取り出した数字のリストを演算木に設定する
                    tree.assingNumbers(numberPerm.next());
                    
                    try {
                        // この時点で演算木が計算可能な状態となっているので計算する
                        Fraction result = tree.evaluate();
                        
                        // 分母:1 分子:10 なら10にできた
                        if (result.denominator == 1 && result.numerator == 10) {
                            System.out.println(join(input) + ": " + tree);
                            if (dump) dump(tree);
                            if (!all) return;
                        }
                    } catch (ZeroDenominatorException e) {
                        // 0除算が発生した場合は10を作れないのでそのまま続行
                        //System.out.println(e);
                    }
                }
            }
            
        } while (tree.nextTree());  // 演算木をひとつ次の構造に変化させる
    }

    private static <T> String join(List<T> list) {
        StringBuilder sb = new StringBuilder();
        for (T i : list) {
            sb.append(i);
        }
        return sb.toString();
    }

    private static void dump(Node tree) {
        System.out.println();
        for (String s : tree.dump()) {
            System.out.println(s);
        }
        System.out.println();
        System.out.println("----------------------------------------");
    }

    private static void make10rpn(List<Integer> input, boolean all, boolean dump) {
        int nNumbers = input.size();
        int nOperators = nNumbers - 1;
        
        if (nOperators == 0) {
            return;
        }
        
        // 演算子の重複組み合せ
        Iterator<List<Operator>> operatorComb = new RepeatedCombinationIterator<>(Operator.values(), nOperators);
        while (operatorComb.hasNext()) {
            
            // 数字と演算子を区別せずにひとつのリストに入れ、そこから順列を作る。
            List<Object> inputAndOperators = new ArrayList<>();
            inputAndOperators.addAll(input);
            inputAndOperators.addAll(operatorComb.next());
            Iterator<List<Object>> rpnPerm = new PermutationIterator<>(inputAndOperators);
            while (rpnPerm.hasNext()) {
                
                // 数字と演算子を区別せずに作った順列を逆ポーランド記法と見なして解く。
                // そうすると逆ポーランド記法として正しくない式も作られる（計算過程でエラーになる）ので、その場合は無視する。
                List<Object> rpn = rpnPerm.next();
                Deque<Fraction> stack = new ArrayDeque<>();
                try {
                    for (Object o : rpn) {
                        if (o instanceof Integer) {
                            stack.push(new Fraction((Integer)o, 1));
                        } else /*if (o instanceof Operator)*/ {
                            Fraction right = stack.pop();
                            Fraction left = stack.pop();
                            stack.push(((Operator)o).calculate(left, right));
                        }
                    }
                    if (stack.size() == 1) {
                        Fraction result = stack.pop();
                        if (result.denominator == 1 && result.numerator == 10) {
                            System.out.println(join(input) + ": " + rpnToEquation(rpn));
                            if (dump) rpnDump(rpn);
                            if (!all) return;
                        }
                    } else {
                        // 終了時、stackの要素数が1以外 = 逆ポーランド記法として正しくない式
                        // （しかし、たぶんここに来る前にNoSuchElementExceptionが出るはず）
                    }
                } catch (NoSuchElementException e) {
                    // stack.pop() により発生 = 逆ポーランド記法として正しくない式
                } catch (ZeroDenominatorException e) {
                    // 0除算が発生した場合は10を作れないのでそのまま続行
                }
            }
        }
    }

    private static String rpnToEquation(List<Object> rpn) {
        Deque<Object> stack = new ArrayDeque<>();
        for (Object o : rpn) {
            if (o instanceof Integer) {
                stack.push((Integer)o);
            } else /*if (o instanceof Operator)*/ {
                Object right = stack.pop();
                Object left = stack.pop();
                stack.push(String.format("(%s %s %s)", left, o, right));
            }
        }
        return (String)stack.pop();
    }

    private static void rpnDump(List<Object> rpn) {
        StringBuilder sb = new StringBuilder();
        for (Object o : rpn) {
            sb.append(o);
        }
        for (int i = 0; i < 32; ++i) {
            sb.insert(0, " ");
        }
        System.out.println(sb);
        
        Deque<Fraction> stack = new ArrayDeque<>();
        for (int i = 0; i < rpn.size(); ++i) {
            Object o = rpn.get(i);
            if (o instanceof Integer) {
                stack.push(new Fraction((Integer)o, 1));
            } else /*if (o instanceof Operator)*/ {
                Fraction right = stack.pop();
                Fraction left = stack.pop();
                stack.push(((Operator)o).calculate(left, right));
            }
            sb.setLength(0);
            for (Fraction f : stack) {
                sb.insert(0, String.format("[%s]", f));
            }
            while (sb.length() < 32+i+1) {
                sb.append(" ");
            }
            for (int j = i+1; j < rpn.size(); ++j) {
                sb.append(rpn.get(j));
            }
            System.out.println(sb);
        }
        System.out.println("----------------------------------------");
    }

}
