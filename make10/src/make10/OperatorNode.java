package make10;

import java.util.List;

/**
 * 演算子ノード
 */
public class OperatorNode extends Node {

    private Operator operator;
    private Node left;
    private Node right;

    public OperatorNode(int nOperators) {
        super(nOperators);
        if (nOperators < 1) {
            throw new IllegalArgumentException();
        }
        makeSubTrees(nOperators-1, 0);
    }

    /**
     * 左右のサブ演算木を生成する。
     * @param leftOperators 左のサブ演算木の演算子数
     * @param rightOperators 右のサブ演算木の演算子数
     */
    private void makeSubTrees(int leftOperators, int rightOperators) {
        left  = (leftOperators  == 0) ? new NumberNode() : new OperatorNode(leftOperators);
        right = (rightOperators == 0) ? new NumberNode() : new OperatorNode(rightOperators);
    }

    @Override
    public boolean nextTree() {
        // nOperatorsが1の場合、このノードの子ノードはどちらも NumberNode なので何もできない。
        if (nOperators == 1) return false;
        
        // 左の木を先に変化させ、左がこれ以上変化できない場合は右の木を変化させる。
        if (left.nextTree() || right.nextTree()) return true;
        
        // 左右の木がこれ以上変化できない場合、
        // 左の木に演算子が1つ以上あれば、左を1つ減らし右を1つ増やして木を作り直す。
        if (left.nOperators > 0) {
            makeSubTrees(left.nOperators-1, right.nOperators+1);
            return true;
        }
        
        // 演算子が全て右の木に移動し、右の木がこれ以上変化できなくなると終了。
        return false;
    }

    @Override
    public List<Integer> assingNumbers(List<Integer> numbers) {
        numbers = left.assingNumbers(numbers);
        return right.assingNumbers(numbers);
    }

    @Override
    public List<Operator> assignOperators(List<Operator> operators) {
        operators = left.assignOperators(operators);
        operators = right.assignOperators(operators);
        operator = operators.get(0);
        return operators.subList(1, operators.size());
    }

    @Override
    public Fraction evaluate() {
        return operator.calculate(left.evaluate(), right.evaluate());
    }

    @Override
    public String toString() {
        return String.format("(%s %s %s)", left, operator, right);
    }

    @Override
    public String[] dump() {
        String[] ldump = left.dump();
        String[] rdump = right.dump();
        String[] dump = new String[1 + rdump.length + ldump.length];

        int i = 0;
        boolean f = true;
        for (int j = 0; j < rdump.length; ++j) {
            dump[i++] = "  " + (!f ? "│" : (f = (rdump[j].charAt(0) != '─')) ? " " : "┌") + rdump[j];
        }
        dump[i++] = "─(" + operator + ")";
        f = true;
        for (int j = 0; j < ldump.length; ++j) {
            dump[i++] = "  " + (!f ? " " : (f = (ldump[j].charAt(0) != '─')) ? "│" : "└") + ldump[j];
        }
        return dump;
    }

}
