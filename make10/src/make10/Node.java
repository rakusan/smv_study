package make10;

import java.util.List;

/**
 * 演算木を構成するノードの抽象クラス
 */
public abstract class Node {

    public final int nOperators;

    /**
     * OperatorNode 用のコンストラクタ
     * @param nOperators このノード以下（このノード自身も含む）のサブ演算木に存在する演算子の数。
     */
    public Node(int nOperators) {
        this.nOperators = nOperators;
    }

    /**
     * NumberNode 用のコンストラクタ
     */
    public Node() {
        this(0);
    }

    /**
     * このノード以下のサブ演算木を、ひとつ次の構造に変化させる。
     * @return 変化できた場合は true 変化できなかった（次の構造がない）場合は false
     */
    public boolean nextTree() {
        return false;
    }

    /**
     * このノード以下のサブ演算木に存在する NumberNode に数字を割り当てる。
     * @param numbers 割り当てる数字のリスト
     * @return 割り当てた数字を除いた残りの数字のリスト
     */
    public List<Integer> assingNumbers(List<Integer> numbers) {
        return numbers;
    }

    /**
     * このノード以下のサブ演算木に存在する OperatorNode に演算子を割り当てる。
     * @param operators 割り当てる演算子のリスト
     * @return 割り当てた演算子を除いた残りの演算子のリスト
     */
    public List<Operator> assignOperators(List<Operator> operators) {
        return operators;
    }

    /**
     * このノード以下の全ての演算を行い、結果の Fraction オブジェクトを返す。
     * @return 演算結果の Fraction オブジェクト
     */
    public abstract Fraction evaluate();

    /**
     * このノード以下のサブ演算木の構造と、演算子および数字の割り当てを視覚的に確認するための文字列配列を生成する。
     */
    public abstract String[] dump();

}
