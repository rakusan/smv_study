package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * 順列を列挙するイテレータ
 */
public class PermutationIterator<T> implements Iterator<List<T>> {

    private final List<T> next;
    private final int n;
    private final int r;
    private final int[] rotate;
    private int i;

    /**
     * items から（重複を許さずに）r 回の選択をする、順列イテレータを作成する。
     * @param items 順列を構成する要素
     * @param r 選択の回数
     */
    public PermutationIterator(List<T> items, int r) {
        if (r > items.size() || r < 0) {
            throw new IllegalArgumentException();
        }
        
        this.next = new ArrayList<>(items);
        this.n = items.size();
        this.r = r;
        
        rotate = new int[Math.min(Math.max(1, n-1), r)];
        i = rotate.length - 1;
    }

    public PermutationIterator(List<T> items) {
        this(items, items.size());
    }

    public PermutationIterator(T[] items, int r) {
        this(Arrays.asList(items), r);
    }

    public PermutationIterator(T[] items) {
        this(items, items.length);
    }

    @Override
    public boolean hasNext() {
        return (i >= 0);
    }

    @Override
    public List<T> next() {
        List<T> result = new ArrayList<>(next);
        if (r < n) {
            result = result.subList(0, r);
        }
        
        while (i >= 0) {
            List<T> sub = next.subList(i, n);
            Collections.rotate(sub, 1);
            rotate[i] = (rotate[i]+1) % (n-i);
            if (rotate[i] == 0) {
                --i;
            } else {
                i = rotate.length - 1;
                break;
            }
        }
        
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
