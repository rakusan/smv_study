package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * 重複組み合せを列挙するイテレータ
 */
public class RepeatedCombinationIterator<T> implements Iterator<List<T>> {

    private final List<T> items;
    private final int n;
    private final int r;
    private final int[] next;

    /**
     * items から重複を許して r 回の選択をする、重複組み合せイテレータを作成する。
     * @param items 重複組み合せを構成する要素
     * @param r 選択の回数
     */
    public RepeatedCombinationIterator(List<T> items, int r) {
        if (r < 0) {
            throw new IllegalArgumentException();
        }
        
        this.items = items;
        this.r = r;
        
        n = items.size();
        next = (r > 0) ? new int[r] : new int[]{n};
    }

    public RepeatedCombinationIterator(T[] items, int r) {
        this(Arrays.asList(items), r);
    }

    @Override
    public boolean hasNext() {
        return (next[0] < n);
    }

    @Override
    public List<T> next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        
        List<T> result = new ArrayList<>(r);
        for (int i : next) {
            result.add(items.get(i));
        }
        
        if (next[0] < n-1) {
            if (next[r-1] < n-1) {
                ++next[r-1];
                return result;
            }
            for (int i = r-2; i >= 0; --i) {
                if (next[i] < n-1) {
                    Arrays.fill(next, i, r, next[i]+1);
                    return result;
                }
            }
        } else {
            //Arrays.fill(next, n);
            next[0] = n;
        }
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
