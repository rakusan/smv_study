package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * 重複順列を列挙するイテレータ
 */
public class RepeatedPermutationIterator<T> implements Iterator<List<T>> {

    private final List<T> items;
    private final int n;
    private final int r;
    private int next;

    /**
     * items から重複を許して r 回の選択をする、重複順列イテレータを作成する。
     * @param items 重複順列を構成する要素
     * @param r 選択の回数
     */
    public RepeatedPermutationIterator(List<T> items, int r) {
        if (r < 0) {
            throw new IllegalArgumentException();
        }
        
        this.items = items;
        this.n = items.size();
        this.r = r;
        
        if (r > 0) {
            double pow = Math.pow(n, r);
            //if (pow > Integer.MAX_VALUE + Math.ulp((double)Integer.MAX_VALUE)) {
            if (pow > Integer.MAX_VALUE + 0.5) {
                throw new UnsupportedOperationException(
                        "too many number of permutations");
            }
            next = (int)Math.round(pow) - 1;
        } else {
            next = -1;
        }
    }

    public RepeatedPermutationIterator(T[] items, int r) {
        this(Arrays.asList(items), r);
    }

    @Override
    public boolean hasNext() {
        return (next >= 0);
    }

    @Override
    public List<T> next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }

        List<T> result = new ArrayList<>(r);
        int i = next--;
        for (int j = 0; j < r; ++j) {
            result.add(items.get(i % n));
            i /= n;
        }
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
