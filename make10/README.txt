binディレクトリに移動して次のように実行する。

  java make10.Make10 <パラメータ>


[実行例]

1158を解く:
  java make10.Make10 1158

4桁の問題を全て解く:
  java make10.Make10 -4

逆ポーランド記法で解く:
  java make10.Make10 -rpn 1158

  (rpnはReverse Polish Notationの略)

演算木または逆ポーランド記法の計算過程を表示する:
  java make10.Make10 -dump 1158
  java make10.Make10 -rpn -dump 1158
