import java.math.*;
import java.util.*;

public class RSA {
    public static void main(String[] args) {
        if (args.length != 2 && args.length != 3) {
            System.err.println("usage 1: java RSA input p q");
            System.err.println("  input: 暗号化したいデータ。p*qより小さい値。");
            System.err.println("   p, q: 秘密鍵を作るための適当な素数。");
            System.err.println();
            System.err.println("usage 2: java RSA input bits");
            System.err.println("  input: 暗号化したいデータ。p*qより小さい値。");
            System.err.println("   bits: 鍵の長さ。秘密鍵をランダムに生成します。");
            System.exit(1);
        }

        BigInteger input = new BigInteger(args[0]);
        BigInteger q, p;

        if (args.length == 2) {
            int bits = Integer.parseInt(args[1]);
            Random rnd = new Random();
            p = BigInteger.probablePrime(bits, rnd);
            q = BigInteger.probablePrime(bits, rnd);
        } else {
            p = new BigInteger(args[1]);
            q = new BigInteger(args[2]);
        }

        BigInteger n = p.multiply(q);   // p*q
        BigInteger one = new BigInteger("1");

        BigInteger e = new BigInteger("65537");
        //BigInteger e = new BigInteger("11");
        BigInteger d = e.modInverse(p.subtract(one).multiply(q.subtract(one))); // (p-1)*(q-1)

        System.out.println("p: " + p);
        System.out.println("q: " + q);
        System.out.println("n: " + n);
        System.out.println("e: " + e);
        System.out.println("d: " + d);

        BigInteger encrypted = input.modPow(e, n);
        BigInteger decrypted = encrypted.modPow(d, n);
        System.out.println("input: " + input);
        System.out.println("暗号化: " + encrypted);
        System.out.println("復号化: " + decrypted);
    }
}
