import java.net.*;
import java.io.*;

public class TCPServer {

    public static void main(String[] args) throws IOException {
        int port = 8888;
        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
            }
        }

        InetSocketAddress address = new InetSocketAddress(port);
        ServerSocket serverSocket = new ServerSocket();

        serverSocket.bind(address);
        System.out.println("Listening on port " + port);

        for (;;) {
            final Socket socket = serverSocket.accept();

            final String from = socket.getInetAddress().getHostAddress();
            System.out.println("Connected from " + from);
        
            Thread t = new Thread() {
                public void run() {
                    try {
                        BufferedReader r = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter w = new PrintWriter(socket.getOutputStream());

                        for (String line; (line = r.readLine()) != null; ) {
                            System.out.printf("[%s] %s%n", from, line);
                            w.println(line.toUpperCase());
                            w.flush();
                        }

                        r.close();
                        w.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Disconnected from " + from);
                }
            };
            t.start();
        }
    }

}
