#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>

void* thread_func(void* arg) {
    int clientfd = (int)arg;
    for (char b; read(clientfd, &b, 1); ) {
        b = toupper(b);
        write(clientfd, &b, 1);
    }
    return NULL;
}

int main() {
    int serverfd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(8888);
    server.sin_addr.s_addr = INADDR_ANY;
    bind(serverfd, (struct sockaddr*)&server, sizeof(server));

    listen(serverfd, 50);
    printf("Listening on port %d\n", ntohs(server.sin_port));

    for (;;) {
        struct sockaddr_in client;
        unsigned int len;
        int clientfd = accept(serverfd, (struct sockaddr *)&client, &len);

        pthread_t thread;
        pthread_create(&thread, NULL, thread_func, (void*)(long)clientfd);
    }
}
