#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    if (argc != 3) {
        fprintf(stderr, "usage: %s command1 command2\n", argv[0]);
        exit(1);
    }

    // パイプの作成
    int pipe_fds[2];
    if (pipe(pipe_fds) == -1) {
        perror("pipe");
        exit(1);
    }

    // わかりやすいように、読み込みと書き込みを別の変数に入れているだけ。
    int read_fd = pipe_fds[0];
    int write_fd = pipe_fds[1];

    int pid1;
    if ((pid1 = fork()) == -1) {
        perror("fork");
        exit(1);
    } else if (pid1 == 0) {
        // 子プロセス1

        // 標準出力にパイプの書き込み側を繋ぐ。
        if (dup2(write_fd, STDOUT_FILENO) == -1) {
            perror("dup2");
            exit(1);
        }

        // ひとつ目のコマンドにパイプの読み込み側は不要なので閉じる。
        close(read_fd);

        // ひとつ目のコマンドでプロセスを置換する。
        char* cmd1[] = { argv[1], NULL };
        execvp(argv[1], cmd1);

        // execvpが成功したらプロセスが置き換わるので、ここには到達しない。
        // ここに来たときは失敗している。
        perror("execvp");
        exit(1);
    }

    int pid2;
    if ((pid2 = fork()) == -1) {
        perror("fork");
        exit(1);
    } else if (pid2 == 0) {
        // 子プロセス2

        // 以下、子プロセス1とほぼ同じだが、
        // パイプの書き込みと読み込み、標準出力と標準入力が逆になっている。

        if (dup2(read_fd, STDIN_FILENO) == -1) {
            perror("dup2");
            exit(1);
        }

        close(write_fd);

        char* cmd2[] = { argv[2], NULL };
        execvp(argv[2], cmd2);
        perror("execvp");
        exit(1);
    }

    // 親プロセスはパイプの読み書きしないので閉じる。
    close(read_fd);
    close(write_fd);

    // 子プロセスの終了を待機する。
    int status;
    while (wait(&status) != -1) {}
}
