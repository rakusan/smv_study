#include <stdio.h>
#include <stdlib.h>

struct HTTPRequest_ {
    char method;  // 0:GET, 1:HEAD, 2:POST
    char *path;
};
typedef struct HTTPRequest_ HTTPRequest;

void service(FILE *in, FILE *out);
HTTPRequest * read_request(FILE *in);
void read_request_line(HTTPRequest *req, FILE *in);
void respond_to(HTTPRequest *req, FILE *out);
void free_request(HTTPRequest *req);

int main()
{
    //printf("HTTP/1.0 200 OK\r\n");
    //printf("Content-Type: text/html; charset=ISO-8859-1\r\n");
    //printf("\r\n");
    //printf("<html><body><h1>Hello!</h1></body></html>");

    service(stdin, stdout);
}

void service(FILE* in, FILE* out)
{
    //リクエストを受け取る
    //リクエストを解析する
    HTTPRequest *req = read_request(in);

    //返すものを特定する
    //返す
    respond_to(req, out);

    free_request(req);
}

HTTPRequest * read_request(FILE *in)
{
    HTTPRequest *req = malloc(sizeof(HTTPRequest));

    // リクエストラインを読む
    read_request_line(req, in);

    // 空行までリクエストヘッダを読む
    // POSTの場合はリクエストボディを読む

    
    
    return req;
}

void read_request_line(HTTPRequest *req, FILE *in)
{
    // TODO
}

void respond_to(HTTPRequest *req, FILE *out)
{
    // TODO
}

void free_request(HTTPRequest *req)
{
    // TODO
}
