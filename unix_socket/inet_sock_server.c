#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <strings.h>
#include <errno.h>

int main()
{
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        perror("socket");
        exit(1);
    }

    struct sockaddr_in server = {0};
    server.sin_family = AF_INET;
    server.sin_port = htons(8888);
    server.sin_addr.s_addr = INADDR_ANY;

    if (bind(sock, (struct sockaddr *)&server, sizeof(server)) == -1) {
        perror("bind");
        exit(1);
    }

    if (listen(sock, 10) == -1) {
        perror("listen");
        exit(1);
    }

    for (int i = 0;;) {
        int fd = accept(sock, NULL, NULL);
        printf(".");
        fflush(stdout);

        char data[7];
        sprintf(data, "test%d\n", (i++)%10);
        write(fd, data, sizeof(data));

        close(fd);
    }
}
