(function() {

function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
}

function clampedIndex(x, y, width, height) {
    return (clamp(y, 0, height-1) * width + clamp(x, 0, width-1)) * 4;
}

/*
 * コンボリューション
 */
function convolution(srcImageData, dstImageData, kernel) {
    var sdata = srcImageData.data;
    var ddata = dstImageData.data;
    var width = srcImageData.width;
    var height = srcImageData.height;

    var kernelSize = kernel.length;
    var totalWeight = kernel.totalWeight;

    for (var y = 0; y < height; ++y) {
        for (var x = 0; x < width; ++x) {
            var p;
            var r = 0, g = 0, b = 0;

            for (var i = 0; i < kernelSize; ++i) {
                var k = kernel[i];
                var w = k.weight;
                p = clampedIndex(x+k.xoffset, y+k.yoffset, width, height);
                r += sdata[p  ] * w;
                g += sdata[p+1] * w;
                b += sdata[p+2] * w;
            }

            p = (y * width + x) * 4;
            ddata[p  ] = (r / totalWeight + 0.5)|0;
            ddata[p+1] = (g / totalWeight + 0.5)|0;
            ddata[p+2] = (b / totalWeight + 0.5)|0;
        }
    }
}

/*
 * 1-passボックスブラー用カーネルの生成
 */
function makeKernel_box_1pass(radius) {
    var kernelWidth = radius * 2 + 1;
    var kernelSize = kernelWidth * kernelWidth;
    var kernel = new Array(kernelSize);
    var totalWeight = 0;

    for (var i = 0; i < kernelSize; ++i) {
        var k = {
            xoffset: (i%kernelWidth)-radius,
            yoffset: ((i/kernelWidth)|0)-radius,
            weight: 1
        };
        kernel[i] = k;
        totalWeight += k.weight;
    }
    kernel.totalWeight = totalWeight;

    return kernel;
}

/*
 * 2-passボックスブラー用カーネルの生成
 */
function makeKernel_box_2pass(radius, pass) {
    var kernelSize = radius * 2 + 1;
    var kernel = new Array(kernelSize);
    var totalWeight = 0;

    for (var i = 0; i < kernelSize; ++i) {
        var k = {
            xoffset: (pass===1) ? i-radius : 0,
            yoffset: (pass===2) ? i-radius : 0,
            weight: 1
        };
        kernel[i] = k;
        totalWeight += k.weight;
    }
    kernel.totalWeight = totalWeight;

    return kernel;
}

/*
 * 2-passガウスブラー用カーネルの生成
 */
function makeKernel_gaussian_2pass(radius, pass) {
    var kernelSize = radius * 2 + 1;
    var kernel = new Array(kernelSize);
    var totalWeight = 0;

    var s = radius/3;  // ぼかし半径の端が3σになるように
    var ss = s*s;

    for (var i = 0; i < kernelSize; ++i) {
        var t = i-radius;
        var g = Math.exp(-0.5*t*t/ss);  // ガウス関数

        var k = {
            xoffset: (pass===1) ? i-radius : 0,
            yoffset: (pass===2) ? i-radius : 0,
            weight: g
        };
        kernel[i] = k;
        totalWeight += k.weight;
    }
    kernel.totalWeight = totalWeight;

    return kernel;
}

/*
 * O(1)ボックスブラー
 *   box_fast_pass1
 *   box_fast_pass2
 */

function box_fast_pass1(srcImageData, dstImageData, radius) {
    var sdata = srcImageData.data;
    var ddata = dstImageData.data;
    var width = srcImageData.width;
    var height = srcImageData.height;

    var totalWeight = radius * 2 + 1;

    for (var y = 0; y < height; ++y) {
        var p;
        var r = 0, g = 0, b = 0;
        for (var x = -radius; x <= radius; ++x) {
            p = clampedIndex(x, y, width, height);
            r += sdata[p  ];
            g += sdata[p+1];
            b += sdata[p+2];
        }
        p = y * width * 4;
        ddata[p  ] = (r / totalWeight + 0.5)|0;
        ddata[p+1] = (g / totalWeight + 0.5)|0;
        ddata[p+2] = (b / totalWeight + 0.5)|0;

        for (var x = 1; x < width; ++x) {
            p = clampedIndex(x-radius-1, y, width, height);
            r -= sdata[p  ];
            g -= sdata[p+1];
            b -= sdata[p+2];
            p = clampedIndex(x+radius, y, width, height);
            r += sdata[p  ];
            g += sdata[p+1];
            b += sdata[p+2];

            p = (y * width + x) * 4;
            ddata[p  ] = (r / totalWeight + 0.5)|0;
            ddata[p+1] = (g / totalWeight + 0.5)|0;
            ddata[p+2] = (b / totalWeight + 0.5)|0;
        }
    }
}

function box_fast_pass2(srcImageData, dstImageData, radius) {
    var sdata = srcImageData.data;
    var ddata = dstImageData.data;
    var width = srcImageData.width;
    var height = srcImageData.height;

    var totalWeight = radius * 2 + 1;

    for (var x = 0; x < width; ++x) {
        var p;
        var r = 0, g = 0, b = 0;
        for (var y = -radius; y <= radius; ++y) {
            p = clampedIndex(x, y, width, height);
            r += sdata[p  ];
            g += sdata[p+1];
            b += sdata[p+2];
        }
        p = y * width * 4;
        ddata[p  ] = (r / totalWeight + 0.5)|0;
        ddata[p+1] = (g / totalWeight + 0.5)|0;
        ddata[p+2] = (b / totalWeight + 0.5)|0;

        for (var y = 1; y < width; ++y) {
            p = clampedIndex(x, y-radius-1, width, height);
            r -= sdata[p  ];
            g -= sdata[p+1];
            b -= sdata[p+2];
            p = clampedIndex(x, y+radius, width, height);
            r += sdata[p  ];
            g += sdata[p+1];
            b += sdata[p+2];

            p = (y * width + x) * 4;
            ddata[p  ] = (r / totalWeight + 0.5)|0;
            ddata[p+1] = (g / totalWeight + 0.5)|0;
            ddata[p+2] = (b / totalWeight + 0.5)|0;
        }
    }
}

/*
 * FFT版ガウスブラー
 *   fft2d_init
 *   fft2d_in
 *   fft2d_out
 *   fft2d_forward
 *   fft2d_inverse
 *   fft2d_blur
 */

var g_fft2d;

function fft2d_init() {
    if (!g_fft2d) {
        FFT.init(256);
        FrequencyFilter.init(256);
        g_fft2d = {
            re_r: new Float32Array(256*256),
            re_g: new Float32Array(256*256),
            re_b: new Float32Array(256*256),
            im_r: new Float32Array(256*256),
            im_g: new Float32Array(256*256),
            im_b: new Float32Array(256*256)
        };
    }
    return g_fft2d;
}

function fft2d_in(fft2d, data) {
    var re_r = fft2d.re_r;
    var re_g = fft2d.re_g;
    var re_b = fft2d.re_b;
    var im_r = fft2d.im_r;
    var im_g = fft2d.im_g;
    var im_b = fft2d.im_b;

    for (var y = 0; y < 256; ++y) {
        var i = y*256;
        for (var x = 0; x < 256; ++x) {
            var j = i+x;
            var k = j*4;
            re_r[j] = data[k  ];
            re_g[j] = data[k+1];
            re_b[j] = data[k+2];
            im_r[j] = im_g[j] = im_b[j] = 0;
        }
    }
}

function fft2d_out(fft2d, data) {
    var re_r = fft2d.re_r;
    var re_g = fft2d.re_g;
    var re_b = fft2d.re_b;

    for (var y = 0; y < 256; ++y) {
        var i = y*256;
        for (var x = 0; x < 256; ++x) {
            var j = i+x;
            var k = j*4;
            data[k  ] = (re_r[j] + 0.5)|0;
            data[k+1] = (re_g[j] + 0.5)|0;
            data[k+2] = (re_b[j] + 0.5)|0;
        }
    }
}

function fft2d_forward(fft2d) {
    FFT.fft2d(fft2d.re_r, fft2d.im_r);
    FFT.fft2d(fft2d.re_g, fft2d.im_g);
    FFT.fft2d(fft2d.re_b, fft2d.im_b);
    FrequencyFilter.swap(fft2d.re_r, fft2d.im_r);
    FrequencyFilter.swap(fft2d.re_g, fft2d.im_g);
    FrequencyFilter.swap(fft2d.re_b, fft2d.im_b);
}

function fft2d_inverse(fft2d) {
    FrequencyFilter.swap(fft2d.re_r, fft2d.im_r);
    FrequencyFilter.swap(fft2d.re_g, fft2d.im_g);
    FrequencyFilter.swap(fft2d.re_b, fft2d.im_b);
    FFT.ifft2d(fft2d.re_r, fft2d.im_r);
    FFT.ifft2d(fft2d.re_g, fft2d.im_g);
    FFT.ifft2d(fft2d.re_b, fft2d.im_b);
}

function fft2d_blur(fft2d, radius, w, h) {
    var re_r = fft2d.re_r;
    var re_g = fft2d.re_g;
    var re_b = fft2d.re_b;
    var im_r = fft2d.im_r;
    var im_g = fft2d.im_g;
    var im_b = fft2d.im_b;

    var s = 128/radius;
    var ss = s*s;

    var sx = w/256;
    var sy = h/256;

    for (var y = 0; y < 256; ++y) {
        var i = y*256;
        for (var x = 0; x < 256; ++x) {
            var j = i+x
            var k = j*4;
            var dx = (x-127.5)/sx;
            var dy = (y-127.5)/sy;
            var d = Math.sqrt(dx*dx + dy*dy);
            var g = Math.exp(-0.5*d*d/ss);
            re_r[j] *= g;
            re_g[j] *= g;
            re_b[j] *= g;
            im_r[j] *= g;
            im_g[j] *= g;
            im_b[j] *= g;
        }
    }
}

/*
 * 作業用のImageData
 */

var tmpCanvas = document.createElement("canvas");
var tmpCtx = tmpCanvas.getContext("2d");
var tmpImageData;

function getTmpImageData(imageData) {
    var width = imageData.width;
    var height = imageData.height;
    if (!tmpImageData || tmpImageData.width !== width || tmpImageData.height) {
        tmpCanvas.width = width;
        tmpCanvas.height = width;
        tmpCtx.fillStyle = "black";  // アルファチャンネルが255で埋められている必要があるので。
        tmpCtx.fillRect(0, 0, width, height);
        tmpImageData = tmpCtx.getImageData(0, 0, width, height);
    }
    return tmpImageData;
}

/*
 * 各実装のエントリーポイント
 */

function box_1pass(imageData, radius) {
    var tmpImageData = getTmpImageData(imageData);
    convolution(imageData, tmpImageData, makeKernel_box_1pass(radius));
    return tmpImageData;
}

function box_2pass(imageData, radius) {
    var tmpImageData = getTmpImageData(imageData);
    convolution(imageData, tmpImageData, makeKernel_box_2pass(radius, 1));
    convolution(tmpImageData, imageData, makeKernel_box_2pass(radius, 2));
    return imageData;
}

function box_fast(imageData, radius) {
    var tmpImageData = getTmpImageData(imageData);
    box_fast_pass1(imageData, tmpImageData, radius);
    box_fast_pass2(tmpImageData, imageData, radius);
    return imageData;
}

function gaussian_2pass(imageData, radius) {
    var tmpImageData = getTmpImageData(imageData);
    convolution(imageData, tmpImageData, makeKernel_gaussian_2pass(radius, 1));
    convolution(tmpImageData, imageData, makeKernel_gaussian_2pass(radius, 2));
    return imageData;
}

function gaussian_fft(imageData, radius, w, h) {
    var data = imageData.data;
    var fft2d = fft2d_init();

    fft2d_in(fft2d, data);
    fft2d_forward(fft2d);
    fft2d_blur(fft2d, radius, w, h);
    fft2d_inverse(fft2d);
    fft2d_out(fft2d, data);

    return imageData;
}

window["blurImpls"] = {
    "box-1pass"      : box_1pass,
    "box-2pass"      : box_2pass,
    "box-fast"       : box_fast,
    "gaussian-2pass" : gaussian_2pass,
    "gaussian-fft"   : gaussian_fft
};

})();
